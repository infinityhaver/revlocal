# README #

This is a WordPress child theme that includes [Bootstrap](http://getbootstrap.com/), [Font Awesome](http://fortawesome.github.io/Font-Awesome/), custom navigation walkers, and an "update-proof" directory for [WooCommerce](http://wordpress.org/plugins/woocommerce/).  You need to install the [WooCommerce plugin](http://wordpress.org/plugins/woocommerce/) in order for the theme to function in it's entirety.  ALSO, this is a **CHILD THEME**** that depends ENTIRELY on the WordPress theme framework [Divi](http://www.elegantthemes.com/gallery/divi/), created by [Elegant Themes](http://www.elegantthemes.com/gallery/divi/)... Unfortunately, the theme is not free.  While I do not condone piracy in anyway and am not responsible for your own less-than-stellar derps, I'm sure an enterprising human unit, such as yourself, might be able to [figure things out on your own](http://kickass.to/usearch/wordpress%20divi/).

* This repo was created to make my life a bit easier to maintain and track.
* Version:  1.0.0

### How do I get set up? ###

* Download this repo, uncompress it, and upload the entirety to your WordPress theme directory and then login to your dashboard and activate the theme. Install [WooCommerce plugin](http://wordpress.org/plugins/woocommerce/) and activate the plugin.
* Or you can be a developer and create a local and/or remote GIT repo and clone it... like you're supposed to.
* Configuration:  I'll get to this part eventually.
* Dependencies: [Divi](http://www.elegantthemes.com/gallery/divi/), [WooCommerce](http://wordpress.org/plugins/woocommerce/)
* Database configuration: Nothing special.  If you're in PHPMyAdmin you're doing it wrong.
* How to run tests:  Srsly... a dev environment.  You are no developer if you only use FTP.
* Deployment instructions: Gonna get to this one too.

### Contribution guidelines ###

* Code review:  I will personally review all push requests and add them to the appropriate branch upon proper submission and syntax.


### Who do I talk to? ###

* Repo owner and admin: [Me](https://bitbucket.org/infinityhaver).