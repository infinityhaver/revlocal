				<?php
					if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>
					<footer id="main-footer">
						<?php get_sidebar( 'footer' ); ?>
						<?php if ( has_nav_menu( 'footer-menu' ) ) : ?>
							<div id="et-footer-nav">
								<div class="container">
									<?php
										wp_nav_menu( array(
											'theme_location' => 'footer-menu',
											'depth'          => '1',
											'menu_class'     => 'bottom-nav',
											'container'      => '',
											'fallback_cb'    => '',
										));
									?>
								</div><!-- .container -->
							</div> <!-- #et-footer-nav -->
						<?php endif; ?>
						<div id="footer-bottom">
							<div class="container clearfix">
								<?php
									if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
										get_template_part( 'includes/social_icons', 'footer' );
									}
								?>
								<p id="footer-info">
									&copy; <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a> | <a href="<?php echo esc_url( home_url( '/sitemap' ) ); ?>">Sitemap</a>
								</p>
							</div>	<!-- .container -->
						</div><!-- #footer-bottom -->
					</footer> <!-- #main-footer -->
				</div> <!-- #et-main-area -->
			<?php endif; ?>
		</div> <!-- #page-container -->
		<?php wp_footer(); ?>
	</body>
</html>