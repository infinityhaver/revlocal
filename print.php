<?php
/*
Template Name: Print
*/

get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() ); ?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->


				<?php
					if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>
					<footer id="main-footer">
						<?php get_sidebar( 'footer' ); ?>
						<?php if ( has_nav_menu( 'footer-menu' ) ) : ?>
							<div id="et-footer-nav">
								<div class="container">
									<?php
										wp_nav_menu( array(
											'theme_location' => 'footer-menu',
											'depth'          => '1',
											'menu_class'     => 'bottom-nav',
											'container'      => '',
											'fallback_cb'    => '',
										));
									?>
								</div><!-- .container -->
							</div> <!-- #et-footer-nav -->
						<?php endif; ?>
						<div id="footer-bottom">
							<div class="container clearfix">
								<?php
									if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
										get_template_part( 'includes/social_icons', 'footer' );
									}
								?>
								<p id="footer-info">
									&copy; <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a> | <a href="<?php echo esc_url( home_url( '/sitemap' ) ); ?>">Sitemap</a>
								</p>
							</div>	<!-- .container -->
						</div><!-- #footer-bottom -->
					</footer> <!-- #main-footer -->
				</div> <!-- #et-main-area -->
			<?php endif; ?>
		</div> <!-- #page-container -->
		<script>

window.print()

</script>
		<?php wp_footer(); ?>
	</body>
</html>