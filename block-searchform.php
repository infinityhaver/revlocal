<form role="search" class="navbar-form navbar-right" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	<div class="form-group">
		<input type="text" class="form-control" value="" name="s" id="s" />
		<button type="submit" id="searchsubmit" class="btn btn-default" value="Search">Search</button>
	</div><!-- -form-group -->
</form>